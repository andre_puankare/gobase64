package main

import (
	"bufio"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

var (
	file               = flag.String("f", "", "file to process")
	encodeDelimeter    = flag.String("ed", "=", "encode key value delimeter")
	decodeDelimeter    = flag.String("dd", ": ", "decode key value delimeter")
	outDelimeter       = flag.String("od", ": ", "out key value delimeter")
	outValueWrapSymbol = flag.String("ovw", "", "out value wrapper symbol")
	inValueWrapSymbol  = flag.String("ivw", "", "in value wrapper symbol")
	decode             = flag.Bool("d", false, "decode file")
)

func main() {
	flag.Parse()

	if *file == "" {
		log.Fatal("argument -f <file> required")
	}

	f, err := os.Open(*file)
	checkError(err)
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()

		switch *decode {
		case true:
			{
				splited := strings.SplitN(line, *decodeDelimeter, 2)
				if !checkSplited(splited, line) {
					continue
				}

				key := splited[0]
				value := strings.Trim(splited[1], *inValueWrapSymbol)

				decodedValue, err := base64.StdEncoding.DecodeString(value)
				if err != nil {
					fmt.Fprintf(os.Stdout, "# Warning: cannot process line: '%s'\n", line)
					continue
				}

				out := fmt.Sprintf(
					"%s%s%s%s%s",
					key,
					*outDelimeter,
					*outValueWrapSymbol,
					string(decodedValue),
					*outValueWrapSymbol,
				)
				fmt.Fprintln(os.Stdout, out)
			}
		case false:
			{
				splited := strings.SplitN(line, *encodeDelimeter, 2)
				if !checkSplited(splited, line) {
					continue
				}
				key := splited[0]
				value := strings.Trim(splited[1], *inValueWrapSymbol)

				encodedValue := base64.StdEncoding.EncodeToString([]byte(value))
				out := fmt.Sprintf(
					"%s%s%s%s%s",
					key,
					*outDelimeter,
					*outValueWrapSymbol,
					encodedValue,
					*outValueWrapSymbol,
				)
				fmt.Fprintln(os.Stdout, out)
			}
		}
	}
}

func checkSplited(splited []string, line string) bool {
	if len(splited) != 2 {
		fmt.Fprintf(os.Stderr, "# Warning: cannot split line: '%s'\n", line)
		return false
	}
	return true
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		os.Exit(1)
	}
}
